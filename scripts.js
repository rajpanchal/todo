//DEFINE UI VARIABLES
const form = document.querySelector("#task-form");
const taskList = document.querySelector(".collection");
const clearBtn = document.querySelector(".clear-tasks");
const filter = document.querySelector("#filter");
const taskInput = document.querySelector("#task");
//const task_li = document.querySelector(".collection-item");

//Load all required Events
loadEventListeners();

function loadEventListeners(){
    //add task event i.e from submit
    form.addEventListener("submit",addTask);
    
    //remove task event (event delegation used)
    taskList.addEventListener("click",removeTask);
    
    //clear all task event
    clearBtn.addEventListener("click", clearTasks);
    
    //filter task event
    filter.addEventListener("keyup",filterTasks);
    
    //document load event
    document.addEventListener("DOMContentLoaded",getTasksFromLS);
}

//Get all tasks from LS
function getTasksFromLS(e){
    //Fetch all tasks from LS
    if(localStorage.getItem("tasks") === null){
        tasks = [];
    }else{
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }
    
    //Create 'n' tasks (li) and append it to ul
    tasks.forEach(function(taskValue){
        //Create a li element
        const li = document.createElement("li");
        li.setAttribute("title", "Edit");
        
        //Add class to li
        li.className = "collection-item";
        
        //Create text node and append it to li
        li.appendChild(document.createTextNode(taskValue));
        
        //Create a new link
        const link = document.createElement("a");
        link.setAttribute("title", "remove");
        
        //Add classes to link
        link.className = "delete-item secondary-content";
        
        //Set href property to link
        link.setAttribute("href", "#");
        
        //Add icon to link
        link.innerHTML = "<i class='fa fa-remove'></i>";
        
        //Append link to li
        li.appendChild(link);
        
        //Adding edit event
        li.addEventListener("click",editTask);
        
        //Append the li to list(ul)
        taskList.appendChild(li);
     });
}

//Add a new task
function addTask(e){
    if(taskInput.value === ''){
        alert("Please insert some task!");
    }else{
        //Create a li element
        const li = document.createElement("li");
        
        li.setAttribute("title", "Edit");
        
        //Add class to li
        li.className = "collection-item";
        
        //Adding edit event to li
        li.addEventListener("click",editTask);
        
        //Create text node and append it to li
        li.appendChild(document.createTextNode(taskInput.value));
        
        //Create a new link
        const link = document.createElement("a");
        link.setAttribute("title", "remove");
        
        //Add classes to link
        link.className = "delete-item secondary-content";
        
        //Set href property of link
        link.setAttribute("href", "#");
        
        //Add icon to link
        link.innerHTML = "<i class='fa fa-remove'></i>"
        
        //Append link to li
        li.appendChild(link);
        
        //Append the li to list(ul)
        taskList.appendChild(li);
        
        //Store to local storage
        storeTaskInLocalStorage(taskInput.value);
        
        //Clear text input
        taskInput.value='';
    }
    e.preventDefault();
}

//Store to local storage
function storeTaskInLocalStorage(taskValue){
    let tasks;
    if(localStorage.getItem("tasks") === null){
        tasks = [];
    }else{
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }
    tasks.push(taskValue);
    
    localStorage.setItem("tasks", JSON.stringify(tasks));
}

function filterTasks(e){
    const text = e.target.value.toLowerCase();
    
    document.querySelectorAll('.collection-item').forEach(function(task){
        const taskValue = task.firstChild.textContent;
        if(taskValue.toLowerCase().indexOf(text) == -1){
            task.style.display = 'none';
        }else{
            task.style.display = 'block';
        }
    });
}

//Clear all tasks
function clearTasks(e){
    //Slower method
    taskList.innerHTML = "";
    
    //Faster method
    while(taskList.firstChild){
        taskList.removeChild(taskList.firstChild);
    }
    
    //Clear the localstorage
    localStorage.clear();
}

//Remove a task
function removeTask(e){
    if(e.target.parentElement.classList.contains("delete-item")){
        if(confirm("Are you sure you want to delete?")){
            //Remove from localstorage
            removeFromLS(e.target.parentElement.parentElement.firstChild.textContent);
            //Remove from browser memory
            e.target.parentElement.parentElement.remove();
        }
    }
}

//Remove it from local storage
function removeFromLS(taskValue){
    //Fetch all tasks from LS
     if(localStorage.getItem("tasks") === null){
        tasks = [];
    }else{
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }
    
    tasks.forEach(function(item,index){
        if(item === taskValue){
            tasks.splice(index, 1);
        }
    });
    
    localStorage.setItem("tasks", JSON.stringify(tasks));
}

//Edit li
var old_data;       // Will keep old data of li
function editTask(e){
    
    const tasks = JSON.parse(localStorage.getItem("tasks"));
    const taskValue = e.target.textContent;
    old_data = taskValue;
    //console.log(tasks);
    //console.log(taskValue);
    
    var curr_indx = tasks.indexOf(taskValue);
    //console.log("curr ind : "+curr_indx);
    
    
    //Replacing li with text field
    
    //Creating new element
    const edit_field = document.createElement("input");
    
    //Add id to edit_field
    edit_field.id = "text-feild"
    edit_field.className = "input-field col s16 teal lighten-5";    
    edit_field.setAttribute("value", taskValue);
    
    //Old
    const old_ele = document.getElementsByClassName("collection-item");
//    Replacig old with new(edit_field)
//    const cardAction = old_ele[curr_indx].parentElement;  //--------SOME MISTAKE-----------
    const cardAction = taskList;
//    console.log("cardAction: " + cardAction);
    cardAction.replaceChild(edit_field, old_ele[curr_indx]);
    
    //Adding text event on edit_field
    edit_field.addEventListener("keydown",saveData);
//    edit_field.addEventListener('blur',saveData);
}

function saveData(e){
    console.log("inside savedata");
    console.log("old_data: " + old_data);
    var new_data = e.target.value;
    if(event.keyCode == 13){
        console.log("entered data: " + new_data);
        
        /******** Creating li and replacing with textfield ********/
        
        //Create a li element
        const li = document.createElement("li");
        
        //Add class to li
        li.className = "collection-item";
        
        //Adding edit event to li
        li.addEventListener("click",editTask);
        
        //Create text node and append it to li
        li.appendChild(document.createTextNode(new_data));
        
        //Create a new link
        const link = document.createElement("a");
        
        //Add classes to link
        link.className = "delete-item secondary-content";
        
        //Set href property of link
        link.setAttribute("href", "#");
        
        //Add icon to link
        link.innerHTML = "<i class='fa fa-remove'></i>"
        
        //Append link to li
        li.appendChild(link);
        
        //Getting old element(text feild)
        const old_ele = document.getElementById("text-feild");
        
        const cardAction = old_ele.parentElement;
        cardAction.replaceChild(li, old_ele);
        
        //Removing old data from LS
        removeFromLS(old_data);
        
        //Store to local storage
        storeTaskInLocalStorage(new_data)
        
        /************************************/
    }
}


