# ToDo
This is simple Todo List using JavaScript POP

## Build With
1. HTML
2. JavaScript

## Plugin Used
1. [Materialize CSS](https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css)

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)